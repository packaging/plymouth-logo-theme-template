#!/bin/sh

create_package() {
TYPE="$1"
fpm -f -s dir -t $TYPE -n "plymouth-theme-${THEME_DIR#*/}" \
--prefix="/usr/share/plymouth/themes" \
--vendor "${VENDOR:-Me}" \
--maintainer "${MAINTAINER:-Myself}" \
--version "$(date +%Y%m%d)~git~$(git rev-parse --short HEAD)" \
--after-install package_scripts/after-install.sh \
--before-remove package_scripts/before-remove.sh \
--depends plymouth-label \
-C ${THEME_DIR%%/*} \
${THEME_DIR#*/}
}

which fpm > /dev/null

if [ $? -eq 0 ]; then
  if [ -d "$1" ]; then
    THEME_DIR="$1"
    sed -i.bak 's,my-logo,'"${THEME_DIR#*/}"',' package_scripts/*.sh
  else
    THEME_DIR="package_root/my-logo"
  fi
  create_package deb
  if [ ! -z $1 ]; then
    rm -f package_scripts/*.sh
    rename 's/.bak//g' package_scripts/*.bak
  fi
else
  echo "fpm not found in PATH"
  exit 1
fi

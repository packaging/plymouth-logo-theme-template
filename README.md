# Plymouth logo template for Ubuntu

It might happen that you'll need to deploy some sort of corporate identity to Ubuntu clients (e.g. in schools, at work, at home, just for fun,...). Therefore it might also be easy, to deploy a package, which takes care of things rather than doing all manually. This package can then be triggerd by some orchestration tools (e.g. Puppet, Salt, Ansible, Chef, whatever,...) or directly when installing via Preseed. So far, so good, here's a simple template derived from the default Ubuntu Logo.

Together with [LightDM Logo template](https://gitlab.com/packaging/lightdm-logo-theme-template/), this will provide a consistent, branded look from boot to login.

![](plymouth.gif)

## Customization

Basically, this one just replaces the logo and adjusts some colors. Best thing is to create a new theme directory in ```package_root``` and adjust things there.

```
MY_CI_TARGET="acme-inc-logo"
cp -R package_root/my-logo package_root/$MY_CI_TARGET
rename 's/my-logo/'"$MY_CI_TARGET"'/' package_root/$MY_CI_TARGET/my-logo*
sed -i 's,my-logo,'"$MY_CI_TARGET"',g' package_root/$MY_CI_TARGET/*.plymouth
```

Now you can replace ```package_root/$MY_CI_TARGET/logo.png``` with yours (transparent PNG makes things look better!).

Also you need to choose at least one color for the background. It is also possible to select 2 and have a smooth gradient from top to bottom. Take your RGB values, divide them by 256 and adjust them in lines 164 and 165 in ```package_root/$MY_CI_TARGET/$MY_CI_TARGET.script```

```
Window.SetBackgroundTopColor (0.2, 0.2, 0.2);     # Nice colour on top of the screen fading to
Window.SetBackgroundBottomColor (0.1, 0.6, 0.6);  # an equally nice colour on the bottom
```

Finally, use the favorite graphics tool of your choice to change the color of ```package_root/$MY_CI_TARGET/progress_dot_on.png``` or use the supplied SVG template (using inkscape for conversation as ImageMagick somehow does not honor the border around the rectangle):

```
sed 's,fill:#000000,fill:'"$MY_FANCY_HEX_COLOR_CODE_FOR_HIGHLIGHT"',' progress_dot_template.svg > /tmp/progress_dot_on.svg
sed 's,fill:#000000,fill:'"$MY_FANCY_HEX_COLOR_CODE_FOR_NON_HIGHLIGHT"',' progress_dot_template.svg > /tmp/progress_dot_off.svg
inkscape /tmp/progress_dot_on.svg --export-png=package_root/$MY_CI_TARGET/progress_dot_on.png
inkscape /tmp/progress_dot_off.svg --export-png=package_root/$MY_CI_TARGET/progress_dot_off.png
```

## Packaging

Just call the script ```create.sh``` with your theme directory as parameter. You should also set the variables ```VENDOR``` and ```MAINTAINER```. You'll need [ruby](http://rvm.io/rvm/install) and the fpm gem (```gem install fpm --no-ri --no-rdoc```). If you don't want to mess up your system, just use Docker:

```
docker run --rm -it -v /tmp/plymouth-logo-theme-template/:/build -w /build ruby:latest /bin/bash
apt-get update && apt-get install ruby ruby-dev -y
gem install fpm --no-ri --no-rdoc
VENDOR="ACME Inc." MAINTAINER="$VENDOR" ./create.sh "package_root/$MY_CI_TARGET"
```
